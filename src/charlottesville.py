"""
Description goes here
"""


from datetime import datetime, timedelta
from functools import partial
from multiprocessing import Pool
from nltk.tokenize import TweetTokenizer, sent_tokenize
from nltk.util import ngrams
from pathlib import Path
from scipy.stats import entropy
import argparse
import errno
import gzip
import logging
import matplotlib.pyplot as plt
import numpy as np
import os
import ujson
import unicodedata


plt.switch_backend('agg')


def main():
    args = make_args()

    logging.basicConfig(filename=str(args.output / 'charlottesville.log'),
                        format='%(asctime)s -> %(message)s',
                        datefmt='%Y/%m/%d %H:%M:%S',
                        level=logging.INFO)

    files = get_tweet_files(args.startdate, args.enddate, args.tweets)

    p = Pool(maxtasksperchild=1)
    ans = p.map(partial(job,
                        outpath=args.output,
                        dictpath=args.dicts,
                        gram=args.gram),
                files,
                chunksize=1)

    fig, axes = plt.subplots(4, 4, figsize=(36, 32))
    dates, H_1, H_1point5, H_2, N_uw, N_tw, zipf_tuples, norm_H, N_tweets = zip(*ans)

    axes[0, 0].plot(dates, H_1, 'b-')
    axes[0, 0].set_xlabel('time', fontsize=25)
    axes[0, 0].set_ylabel('$H$', fontsize=25)

    axes[0, 1].plot(dates, norm_H, '-', color='orange')
    axes[0, 1].set_xlabel('time', fontsize=25)
    axes[0, 1].set_ylabel('$H_{norm}$', fontsize=25)

    reds = np.linspace(0, 1, len(zipf_tuples))
    i = 0
    for tup in zipf_tuples:
        color_tup = (reds[i], 0, 0)
        axes[0, 2].plot(np.log10(tup[0]), np.log10(tup[1]), color=color_tup)
        i += 1
    axes[0, 2].set_xlabel('$\log_{10} r$', fontsize=25)
    axes[0, 2].set_ylabel('$\log_{10} N_r$', fontsize=25)

    axes[1, 0].plot(dates[1:], np.diff(H_1), 'b-')
    axes[1, 0].set_xlabel('time', fontsize=25)
    axes[1, 0].set_ylabel('$dH/dt$', fontsize=25)

    axes[1, 1].plot(dates[1:], np.diff(norm_H), '-', color='orange')
    axes[1, 1].set_xlabel('time', fontsize=25)
    axes[1, 1].set_ylabel("$dH_{norm}/dt$", fontsize=25)

    axes[1, 2].scatter(N_tweets, norm_H)
    axes[1, 2].set_xlabel('$N_{tweets}$', fontsize=25)
    axes[1, 2].set_ylabel('$H_{norm}$', fontsize=25)

    axes[2, 0].plot(dates, N_tweets, 'k-')
    axes[2, 0].set_xlabel('time', fontsize=25)
    axes[2, 0].set_ylabel('$N_{tweets}$', fontsize=25)

    axes[2, 1].plot(dates, N_uw, 'k-', label='Unique words')
    axes[2, 1].set_xlabel('time', fontsize=25)
    axes[2, 1].set_ylabel('Unique words', fontsize=25)

    axes[2, 2].plot(dates, N_tw, 'k-', label='Total words')
    axes[2, 2].set_xlabel('time', fontsize=25)
    axes[2, 2].set_ylabel('Total words', fontsize=25)

    axes[3, 0].scatter(N_uw, H_1, c='k')
    axes[3, 0].set_xlabel('Unique words', fontsize=25)
    axes[3, 0].set_ylabel('$H$', fontsize=25)

    axes[3, 1].scatter(N_tw, H_1, c='k')
    axes[3, 1].set_xlabel('Total words', fontsize=25)
    axes[3, 1].set_ylabel('$H$', fontsize=25)

    axes[3, 2].scatter(N_uw[1:], np.diff(H_1), c='k')
    axes[3, 2].set_xlabel('Unique words', fontsize=25)
    axes[3, 2].set_ylabel('$dH/dt$', fontsize=25)

    axes[3, 3].scatter(N_tw[1:], np.diff(H_1), c='k')
    axes[3, 3].set_xlabel('Total words', fontsize=25)
    axes[3, 3].set_ylabel('$dH/dt$', fontsize=25)

    plt.savefig(str(args.output / (str(args.gram) + 'gram.pdf')))


def make_args():
    description = 'Process tweets into GenSim disctionary.'
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument('-s',
                        '--startdate',
                        help='The start date - format YYYY-MM-DD',
                        required=True,
                        type=valid_date)

    parser.add_argument('-e',
                        '--enddate',
                        help='The end date - format YYYY-MM-DD',
                        required=True,
                        type=valid_date)

    parser.add_argument('-o',
                        '--output',
                        help='The output file path',
                        required=True,
                        type=path_type)

    parser.add_argument('-t',
                        '--tweets',
                        help='The path to the tweets',
                        required=True,
                        type=str)

    parser.add_argument('-g',
                        '--gram',
                        help='What gram to use',
                        required=True,
                        type=int)

    parser.add_argument('-d',
                        '--dicts',
                        help='The path to word dicts (lazy loads).',
                        required=True,
                        type=path_type)

    return parser.parse_args()


def valid_date(d):
    try:
        return datetime.strptime(d, "%Y-%m-%d")

    except ValueError:
        msg = "Invalid date format in provided input: '{}'.".format(d)
        raise argparse.ArgumentTypeError(msg)


def path_type(p):
    path = Path(p)
    path.mkdir(parents=True, exist_ok=True)
    return path


def get_tweet_files(start, end, tweet_path):
    files = []
    while start <= end:
        p = Path(tweet_path) / str(start.date())
        files.extend(p.glob("*.gz"))
        start += timedelta(days=1)

    return sorted(files)


def job(tweet_file, outpath, dictpath, gram):
    p = Path(dictpath) / tweet_file.parts[-2] / tweet_file.stem
    # create path (avoids race conditions)
    try:
        os.makedirs(str(p))

    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

    c = Path(p) / (str(gram) + 'count.json')
    p = Path(p) / (str(gram) + 'gram.json')

    try:
        with open(str(p), 'r') as f:
            dictionary = ujson.load(f)
            logging.info("Dict file loaded: {}\n".format(p))
        with open(str(c), 'r') as cPath:
            count = ujson.load(cPath)

    except FileNotFoundError:
        dictionary, count = make_freq_dict(tweet_file, outpath, gram)
        logging.info("Tweet file processed: {}\n".format(tweet_file))
        with open(str(p), 'w') as f:
            ujson.dump(dictionary, f)
        with open(str(c), 'w') as cPath:
            ujson.dump(count, cPath)

    return (datetime.strptime(tweet_file.stem, "%Y-%m-%d-%H-%M"),
            entropy(list(dictionary.values())),
            gen_entropy(list(dictionary.values()), alpha=1.5),
            gen_entropy(list(dictionary.values()), alpha=2),
            len(dictionary.values()),
            sum(dictionary.values()),
            zipf(dictionary),
            entropy(list(dictionary.values())) /
            np.log(len(list(dictionary.values()))),
            count)


def word2freq(texts):
    d = {}
    for key in texts:
        try:
            d[key] += 1
        except KeyError:
            d[key] = 1
    return d


def make_freq_dict(tweet_file, outpath, gram):
    count = 0
    with gzip.open(str(tweet_file), 'rb') as f:
        texts = []

        for line in f:
            try:
                tweet = ujson.loads(line)

            except ValueError:
                continue

            # Only utilize English tweets
            if not language_check(tweet, language='en'):
                continue

            # Store tweet id for reproducibility
            tweet_id = get_tweet_id(tweet)

            if not tweet_id:
                continue

            with open(str(outpath / 'tweet_ids.txt'), 'a') as id_file:
                id_file.write('{}\n'.format(tweet_id))

            # Get tokenized tweet text
            tokens = []
            for text in get_tweet_text(tweet):
                tokens.extend(tokenize_tweet(text, gram))

            texts.extend(tokens)
            count += 1

        return word2freq(texts), count


def language_check(tweet, language='en'):
    for key in ['lang', 'twitter_lang']:
        try:
            return tweet[key] == language

        except KeyError:
            pass

    return False


def get_tweet_text(tweet):
    text = []

    # Obtain main tweet text
    for key in ['text', 'body']:
        try:
            text.append(tweet[key])

        except KeyError:
            pass

    # Obtain original tweet text for quote tweets
    try:
        text.append(tweet['quoted_status']['text'])

    except KeyError:
        pass

    try:
        text.append(tweet['twitter_quoted_status']['body'])

    except KeyError:
        pass

    return text


def get_tweet_id(tweet):
    if 'id_str' in tweet.keys():
        tweet_id = tweet['id_str']

    elif 'id' in tweet.keys():
        tweet_id = tweet['id'].split(':')[-1]

    else:
        tweet_id = None

    return tweet_id


def tokenize_tweet(tweet_text, gram):
    tweetTok = TweetTokenizer()
    tweet_text = sent_tokenize(tweet_text)
    cats = ['Lm', 'Mn', 'Me', 'Pc', 'Pd', 'Ps', 'Pe', 'Pi', 'Pf', 'Po']
    tokens = []
    for s in tweet_text:
        s = tweetTok.tokenize(s.lower())
        s = [x for x in s if len(x) > 1 or (unicodedata.category(x) not in cats)]
        for ngram in ngrams(s, gram):
            tokens.append(' '.join(ngram))

    return tokens


def gen_entropy(f_i, alpha=1):
    """! Returns the generalized entropy of a frequency / probability dist

    @param f_i (array): frequency or probability distribution estimates.
    Does not need to be normalized.
    @param alpha (float or int): >=0. alpha = 1 gives back Shannon entropy.

    @return gen_entropy (float): -\sum p_i \ln p_i if alpha = 1,
    \sum p_i^alpha otherwise
    """
    if float(alpha) == 1.:
        return entropy(f_i)
    
    else:
        f_i = np.asarray(f_i)
        p_i = 1. * f_i / np.sum(f_i, axis=0)
        return np.sum(p_i**alpha)


def zipf(d):
    """! Gets the rank-frequency information from a text dictionary

    @param d (dict): {word : n_occurences}

    @return zipf (tuple): (rank, frequency) where rank = range(1, n_words + 1)
    and frequency is [largest, second-largest, third-largest,...]
    """
    counts = list(d.values())
    counts.sort()
    return (range(1, len(counts) + 1), counts[::-1])


if __name__ == '__main__':
    main()
