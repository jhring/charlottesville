#!/bin/bash
#PBS -qpoolmemq
#PBS -l nodes=1:ppn=24,mem=24gb,vmem=24gb
#PBS -l walltime=12:00:00
#PBS -N charlottesville
#PBS -j oe
#PBS -M jhring@uvm.edu
#PBS -m bea

cd ~/charlottesville/src
python3 charlottesville.py -s 2017-08-10 -e 2017-08-13 -o ~/charlottesville/output -t /users/c/d/cdanfort/scratch/twitter/tweet-troll/zipped-raw/ -g 1
python3 charlottesville.py -s 2017-08-10 -e 2017-08-13 -o ~/charlottesville/output -t /users/c/d/cdanfort/scratch/twitter/tweet-troll/zipped-raw/ -g 2
python3 charlottesville.py -s 2017-08-10 -e 2017-08-13 -o ~/charlottesville/output -t /users/c/d/cdanfort/scratch/twitter/tweet-troll/zipped-raw/ -g 3

