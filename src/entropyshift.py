# Compares two distributions of word frequencies (csv) and generates an normalized entropy wordshift.

import matplotlib as mpl
mpl.use( "agg" )

import math
import os
import re
import codecs
import copy
import subprocess
from jinja2 import Template
from numpy import unique
import numpy as np
import pandas as pd
import glob
from ast import literal_eval
import matplotlib.pyplot as plt
import matplotlib.font_manager as font_manager

#Reference distribution followed by the test distribution
df=pd.read_csv('Pathto.csv')[['word','frequency']]
df2=pd.read_csv('Pathtoother.csv')[['word','frequency']]


plt.rc('text', usetex=False)
plt.rc('font', family='Serif')

def shift(refFreq,compFreq,words,sort=True):
    """Compute a shift, and return the results.

    If sort=True, will return the three sorted lists, and sumTypes. Else, just the two shift lists, and sumTypes (words don't need to be sorted)."""

    # normalize frequencies
    Nref = float(sum(refFreq))
    Ncomp = float(sum(compFreq))
    for i in range(len(refFreq)):
       refFreq[i] = float(refFreq[i])/Nref
       compFreq[i] = float(compFreq[i])/Ncomp
    # compute the reference entropy
    refH = sum([-(refFreq[i])*np.log2(refFreq[i])/np.log2(len([x for x in refFreq if x>0])) for i in range(len(refFreq))])
    # determine shift magnitude, type
    shiftMag = [0 for i in range(len(refFreq))]
    shiftType = [0 for i in range(len(refFreq))]
    bits = [0 for i in range(len(refFreq))]
    for i in range(len(refFreq)):
        freqDiff = compFreq[i]-refFreq[i]
        bits[i] = compFreq[i]*(-np.log2(compFreq[i])/compFreq[i])
        shiftMag[i] = compFreq[i]*((-np.log2(compFreq[i])/np.log2(len([x for x in compFreq if x>0]))-refH)/compFreq[i])*(freqDiff)
        if math.isnan(shiftMag[i]):
            shiftMag[i]=0

        if freqDiff < 0 and shiftMag[i] < 0:
            shiftType[i] = 0
        elif freqDiff < 0 and shiftMag[i] > 0:
            shiftType[i] = 1
        elif freqDiff > 0 and shiftMag[i] < 0:
            shiftType[i] = 2
        elif freqDiff > 0 and shiftMag[i] > 0:
            shiftType[i] = 3

    indices = sorted(range(len(shiftMag)), key=lambda k: abs(shiftMag[k]), reverse=True)
    sumTypes = [0.0 for i in range(4)]
    for i in range(len(refFreq)):
        sumTypes[shiftType[i]] += shiftMag[i]

    sortedMag = [shiftMag[i] for i in indices]
    sortedType = [shiftType[i] for i in indices]
    sortedWords = [words[i] for i in indices]
    sortedDiff = [compFreq[i]-refFreq[i] for i in indices]
    sortedBits = [bits[i] for i in indices]

    if sort:
        return sortedMag,sortedWords,sortedType,sumTypes,refH,sortedBits,sortedDiff
    else:
        return shiftMag,shiftType,sumTypes


colors={0:'khaki',1:'cornflowerblue',2:'royalblue',3:'gold'}


def f7(seq):
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]


df2.columns=['word','frequency2']
df=pd.merge(df,df2,on='emoji', how='outer', sort=True)
df=df.fillna(0)
df['prob1']=df['frequency'].astype(float)/sum(df['frequency'])
df['prob2']=df['frequency2'].astype(float)/sum(df['frequency2'])
df['bits1']=-df['prob1']*np.log2(df['prob1'])
df['bits2']=-df['prob2']*np.log2(df['prob2'])

sortedMag,sortedWords,sortedType,sumTypes,refH,sortedBits,sortedDiff= shift(df['frequency'].astype(int).tolist(),df['frequency2'].astype(int).tolist(),df['emoji'].tolist(),sort=True)

sortedColors=[colors[x] for x in sortedType]

fig=plt.figure()
ax=plt.subplot(111)
ax.set_yticks(range(20))
ax.set_yticklabels(sortedWords[:20][::-1],fontsize=10)
ax.tick_params(axis='x', which='major', labelsize=7)
ax.grid(b=None, which='major', axis='y',zorder=0)
ax.barh(range(20), sortedMag[:20][::-1],color = sortedColors[:20][::-1],zorder=3)

plt.xlabel('Per emoji average Emojropy shift',fontsize='large')
plt.ylabel('Word',fontsize='large')
plt.title('Wordshift',fontsize='x-large')

# Uncomment this next bit and enter the file path to save the image.
# plt.savefig('/Users/bfemery/Documents/emojropy/Aug27emojropyshift.png',dpi=300)
