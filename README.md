# Description:

This project aims to create a narrative extractor which is able to identify important events and topics in social media, news sites, internet chat rooms, etc.

After identifying narratives, the second goal of this project is to provide a meaningful analysis of different characteristics which may be attached to an individual narrative as well as various comparisons between two or more narratives.

---

# Dependency List:
* {Name}     ({tested version})
* Python     (3.5)
* gensim     (2.3.0)
* nltk       (3.2.3)
* numpy      (1.12.1)
* matplotlib (2.0.2)
* ujson      (1.35)
* scipy      (0.19.0)
* pandas     (0.20.1)

---
